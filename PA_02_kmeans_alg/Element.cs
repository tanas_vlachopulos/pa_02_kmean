﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    public interface Element
    {
        int X { get; set; }
        int Y { get; set; }
    }
}
