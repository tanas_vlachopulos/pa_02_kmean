﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    public class Centroid : Element
    {
        public int X { get; set; }
        public int Y { get; set; }

        public int AvgX { get; set; }
        public int AvgY { get; set; }
        public int AssignedNodes { get; set; }

        private List<Node> nodes;
        public List<Node> Nodes { get { return nodes; } }

        public int Name { get; set; }

        public Centroid(int x, int y)
        {
            this.X = x;
            this.Y = y;
            AvgX = 0;
            AvgY = 0;
            AssignedNodes = 0;
            this.nodes = new List<Node>();
        }

        public void UpdatePosition()
        {
            try
            {
                X = AvgX / AssignedNodes;
                Y = AvgY / AssignedNodes;
                AvgX = 0;
                AvgY = 0;
                AssignedNodes = 0;
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddNode(Node node)
        {
            //this.nodes.Add(node);
        }

        public void FlushNodes()
        {
            this.nodes.Clear();
        }
    }
}
