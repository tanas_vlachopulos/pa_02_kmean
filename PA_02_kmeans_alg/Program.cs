﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PA_02_kmeans_alg
{
    class Program
    {
        private static readonly int nodes_cout = 19999;
        private static readonly int centroi_cout = 32;
        private static readonly int threads_count = 1;

        static void Main(string[] args)
        {
            if (nodes_cout < centroi_cout)
            {
                Console.WriteLine("Count of nodes must by great then count of centroids");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Visual visualView = new Visual();

            int randomSeed = DateTime.Now.Millisecond;
            RandomGenerator random = new RandomGenerator(nodes_cout, randomSeed);
            List<Node> nodes = random.Nodes;
            List<Centroid> centroids = SelectCentroids(nodes, centroi_cout);

            // ****** test **********
            //MultithreadAssignProcess(nodes, centroids, threads_count);
            //foreach (var node in nodes)
            //{
            //    Console.WriteLine("Node ({0}, {1}) >>> Centroid ({2}, {3}) Name: {4}", node.X, node.Y, node.Centroid.X, node.Centroid.Y, node.Centroid.Name);
            //}

            int iteration = 0;
            Stopwatch watch = new Stopwatch();
            watch.Start();

            while (MultithreadAssignProcess(nodes, centroids, threads_count))
            {
                //foreach (Node node in nodes)
                //{
                //    Console.WriteLine("Node ({0}, {1}) >>> Centroid ({2}, {3}) Name: {4}", node.X, node.Y, node.Centroid.X, node.Centroid.Y, node.Centroid.Name);
                //}
                //Console.WriteLine("~~~~~~~~~~~~~~~~ {0} ~~~~~~~~~~~~~~~~~", iteration++);
                iteration++;

                //TransferCentroids(nodes, centroids);
                MultithreadTransferProcess(nodes, centroids, threads_count);
                //MultithreadTransferProcess2(nodes, centroids, 1);

            }

            watch.Stop();
            Console.WriteLine("iteration = {0}", iteration);
            Console.WriteLine("Elapsed time = {0}", watch.Elapsed);

            // visualise data
            if (nodes_cout < 20000)
            {
                visualView.VisualizeData(nodes, centroids, 100);
                Application.Run(visualView);
            }
            else
            {
                Console.ReadKey();
            }

        }

        static List<Centroid> SelectCentroids(List<Node> elements, int count)
        {
            List<Centroid> centroids = new List<Centroid>();
            if (elements.Count >= count)
            {
                for (int i = 0; i < count; i++)
                {
                    Node node = elements.ElementAt(i);
                    centroids.Add(node.ConvertToCentroid());
                    centroids.ElementAt(i).Name = i;
                    //elements.Remove(node);
                }
            }
            return centroids;
        }

        static bool MultithreadAssignProcess(List<Node> nodes, List<Centroid> centroids, int threadsCount)
        {
            List<List<Node>> nodesParts = SplitElementList<Node>(nodes, threadsCount);

            ManualResetEvent[] doneEvents = new ManualResetEvent[threadsCount];
            AssignProcess[] assignPrecesses = new AssignProcess[threadsCount];
            
            for (int i = 0; i < threadsCount; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                AssignProcess process = new AssignProcess(nodesParts.ElementAt(i), centroids, doneEvents[i]);
                assignPrecesses[i] = process;
                ThreadPool.QueueUserWorkItem(process.ThreadPoolCallback, i);
            }

            WaitHandle.WaitAll(doneEvents);

            //foreach (var nodeList in nodesParts)
            //{
            //    Console.WriteLine("********* Node Part ************");
            //    foreach (var node in nodeList)
            //    {
            //        Console.WriteLine("Node ({0}, {1}) >>> Centroid ({2}, {3}) Name: {4}", node.X, node.Y, node.Centroid.X, node.Centroid.Y, node.Centroid.Name);
            //    }
            //}
            //Console.WriteLine("All processes are complete");

            bool reassign = false;
            foreach (AssignProcess process in assignPrecesses)
            {
                // indicate if a node has been assigned to another centroid
                reassign = process.Reassign ? true : reassign;  
            }
            return reassign;
        }

        static bool AssingToCentroids(List<Node> nodes, List<Centroid> centroids)
        {
            // indicate if a node has been assigned to another centroid
            bool reassign = false;

            // calculate distance of all nodes
            foreach (Node node in nodes)
            {
                Centroid nearestCentroid = centroids.ElementAt(0);
                double minDistance = CountDistance(node, nearestCentroid);

                // to all centroids
                foreach (Centroid centroid in centroids)
                {
                    if (minDistance >= CountDistance(node, centroid))
                    {
                        nearestCentroid = centroid;
                        minDistance = CountDistance(node, centroid);
                    }
                }

                if (node.Centroid != nearestCentroid)
                {
                    reassign = true;
                    node.Centroid = nearestCentroid;
                }
            }

            return reassign;
        }

        static void MultithreadTransferProcess(List<Node> nodes, List<Centroid> centroids, int threadsCount)
        {
            List<List<Node>> nodesParts = SplitElementList<Node>(nodes, threadsCount);

            ManualResetEvent[] doneEvents = new ManualResetEvent[threadsCount];
            TransferProcess[] transferProcesses = new TransferProcess[threads_count];

            for (int i = 0; i < threadsCount; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                TransferProcess process = new TransferProcess(nodesParts.ElementAt(i), doneEvents[i]);
                transferProcesses[i] = process;
                ThreadPool.QueueUserWorkItem(process.ThreadPoolCallback, i);
            }

            WaitHandle.WaitAll(doneEvents);

            //foreach (Centroid centroid in centroids)
            //{
            //    centroid.UpdatePosition();
            //}
            Parallel.ForEach(centroids, (centroid) =>
            {
                centroid.UpdatePosition();
            });
        }

        static void MultithreadTransferProcess2(List<Node> nodes, List<Centroid> centroids, int threadsCount)
        {
            List<List<Centroid>> centroidsParts = SplitElementList<Centroid>(centroids, threadsCount);

            ManualResetEvent[] doneEvents = new ManualResetEvent[threadsCount];
            TransferProcess2[] transferProcesses = new TransferProcess2[threads_count];

            for (int i = 0; i < threadsCount; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                TransferProcess2 process = new TransferProcess2(centroidsParts.ElementAt(i), doneEvents[i]);
                transferProcesses[i] = process;
                ThreadPool.QueueUserWorkItem(process.ThreadPoolCallback, i);
            }

            WaitHandle.WaitAll(doneEvents);
        }

        static void TransferCentroids(List<Node> nodes, List<Centroid> centroids)
        {
            foreach (Node node in nodes)
            {
                Centroid centroid = node.Centroid;
                centroid.AvgX += node.X;
                centroid.AvgY += node.Y;
                centroid.AssignedNodes++;
            }

            foreach (Centroid centroid in centroids)
            {
                centroid.UpdatePosition();
            }
        }

        // split list of elements to sublists 
        static List<List<T>> SplitElementList<T>(List<T> elements, int parts)
        {
            List<List<T>> list = new List<List<T>>();
            int blockSize = (int)Math.Ceiling((double)elements.Count / parts);

            for (int i = 0; i < elements.Count; i += blockSize)
            {
                list.Add(elements.GetRange(i, Math.Min(blockSize, elements.Count - i)));
            }

            return list;
        }

        static double CountDistance(Element a, Element b)
        {
            double distX = a.X - b.X, distY = a.Y - b.Y;
            return Math.Sqrt(distX * distX + distY * distY);
        }
    }
}
