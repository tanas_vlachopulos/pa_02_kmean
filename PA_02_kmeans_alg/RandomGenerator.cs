﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    class RandomGenerator
    {
        private List<Node> nodes;

        public List<Node> Nodes { get { return nodes; } }

        public RandomGenerator(int size, int seed)
        {
            Random rand = new Random(seed);
            nodes = new List<Node>();

            for (int i = 0; i < size; i++)
            {
                nodes.Add(new Node(rand.Next(0, 100), rand.Next(0, 100)));
            }
        }
    }
}
