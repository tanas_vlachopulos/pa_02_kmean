﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{

    public class Node : Element
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Centroid Centroid { get; set; }

        public Node(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Centroid = null;
        }

        public Centroid ConvertToCentroid()
        {
            return new Centroid(X, Y);
        }
    }
}
