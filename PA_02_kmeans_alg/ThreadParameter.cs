﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    class ThreadParameter
    {
        public int Size { get; set; }
        public string Name { get; set; }
        public int Seed { get; set; }
    }
}
