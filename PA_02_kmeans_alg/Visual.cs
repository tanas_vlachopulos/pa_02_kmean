﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PA_02_kmeans_alg
{
    public partial class Visual : Form
    {
        private List<Node> nodes;
        private List<Centroid> centroids;
        int maxValue;

        Color[] colors = { Color.Red, Color.Blue, Color.Green, Color.Orange, Color.YellowGreen, Color.Purple, Color.Magenta, Color.DarkGreen, Color.Khaki, Color.Indigo, Color.Cyan, Color.LightBlue };

        public Visual()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Enterpoint for datavisualisation
        /// </summary>
        /// <param name="nodes">list of nodes</param>
        /// <param name="centroids">list of centroids</param>
        /// <param name="maxValue">max value for X and Y position</param>
        public void VisualizeData(List<Node> nodes, List<Centroid> centroids, int maxValue)
        {
            this.centroids = centroids;
            this.nodes = nodes;
            this.maxValue = maxValue;
            panel1_Paint(this, null);
        }

        private void Visual_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            // nodes colors
            Dictionary<int, Color> colorMap = new Dictionary<int, Color>();

            using (Graphics g = canvas.CreateGraphics())
            {
                float range = canvas.Width / maxValue;

                int counter = 0;
                foreach (Centroid centroid in centroids)
                {
                    
                    Color color = colors[counter];

                    counter = counter < colors.Length - 1 ? counter += 1 : 0; // colors list overflow prevention

                    colorMap.Add(centroid.Name, color);

                    Brush b = new SolidBrush(Color.Black);
                    g.FillEllipse(b, range * centroid.X - 6, range * centroid.Y - 6, 12, 12);
                    b.Dispose();

                }

                foreach (Node node in nodes)
                {
                    Brush b = new SolidBrush(colorMap[node.Centroid.Name]);
                    g.FillEllipse(b, range * node.X - 4, range * node.Y - 4, 8, 8);
                    b.Dispose();
                }

            }
        }

        private Color GetRandomColor()
        {
            Random randomGen = new Random(DateTime.Now.Millisecond);
            return this.colors[randomGen.Next(colors.Length)];
        }
    }
}
