﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    class TransferProcess2
    {
        private List<Centroid> centroids;
        private ManualResetEvent doneEvent;

        public TransferProcess2(List<Centroid> centroids, ManualResetEvent doneEvent)
        {
            this.centroids = centroids;
            this.doneEvent = doneEvent;
        }

        public void ThreadPoolCallback(Object context)
        {
            int threadId = (int)context;
            TransferCentroids(centroids);
            Console.WriteLine("Transfer process {0} complete", threadId);
            doneEvent.Set();
        }

        public void TransferCentroids(List<Centroid> centroids)
        {
            foreach (Centroid centroid in centroids)
            {
                foreach (Node node in centroid.Nodes)
                {
                    centroid.AvgX += node.X;
                    centroid.AvgY += node.Y;
                    centroid.AssignedNodes++;
                }

                centroid.UpdatePosition();
                centroid.FlushNodes();
            }
        }

    }
}
