﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    class TransferProcess
    {
        private List<Node> nodes;
        private ManualResetEvent doneEvent;

        public TransferProcess(List<Node> nodes, ManualResetEvent doneEvent)
        {
            this.nodes = nodes;
            this.doneEvent = doneEvent;
        }

        public void ThreadPoolCallback(Object context)
        {
            int threadId = (int)context;
            TransferCentroids(nodes);
            Console.WriteLine("Transfer process {0} complete", threadId);
            doneEvent.Set();
        }

        public void TransferCentroids(List<Node> nodes)
        {
            foreach (Node node in nodes)
            {
                Centroid centroid = node.Centroid;

                lock (centroid)
                {
                    centroid.AvgX += node.X;
                    centroid.AvgY += node.Y;
                    centroid.AssignedNodes++; 
                }
            }
        }
    }
}
