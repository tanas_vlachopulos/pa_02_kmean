﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PA_02_kmeans_alg
{
    class AssignProcess
    {
        private List<Centroid> centroids;
        private List<Node> nodes;
        private bool reassign;
        private ManualResetEvent doneEvent;

        public bool Reassign { get { return reassign; } }

        public AssignProcess(List<Node> nodes, List<Centroid> centroids, ManualResetEvent doneEvent)
        {
            this.centroids = centroids;
            this.nodes = nodes;
            this.reassign = false;
            this.doneEvent = doneEvent;
        }

        public void ThreadPoolCallback(Object context)
        {
            int threadId = (int)context;
            reassign = AssingToCentroids(nodes, centroids);
            Console.WriteLine("Assign process {0} complete", threadId);
            doneEvent.Set();
        }

        public bool AssingToCentroids(List<Node> nodes, List<Centroid> centroids)
        {
            // indicate if a node has been assigned to another centroid
            bool reassign = false;

            // calculate distance of all nodes
            foreach (Node node in nodes)
            {
                Centroid nearestCentroid = centroids.ElementAt(0);
                double minDistance = CountDistance(node, nearestCentroid);

                // to all centroids
                foreach (Centroid centroid in centroids)
                {
                    if (minDistance >= CountDistance(node, centroid))
                    {
                        nearestCentroid = centroid;
                        minDistance = CountDistance(node, centroid);
                    }
                }

                // reassign to centroid
                if (node.Centroid != nearestCentroid)
                {
                    reassign = true;
                    node.Centroid = nearestCentroid;
                    nearestCentroid.AddNode(node);
                }
            }

            return reassign;
        }

        private double CountDistance(Element a, Element b)
        {
            double distX = a.X - b.X, distY = a.Y - b.Y;
            return Math.Sqrt(distX * distX + distY * distY);
        }

    }
}
